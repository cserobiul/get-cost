<?php

class GetCost {
    public $id = "";
    public $date = "";
    public $type = "";
    public $note= "";
    public $amount = "";
    
    public function __construct(){
        session_start();
        define('DBName', 'getcost');
        define('br', '<br/>');
        
        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
        $db = mysql_select_db('getcost') or die(mysql_error());
    }
    public function assign($data = ''){
        $this->date = $data['date'];
        $this->type = $data['tType'];
        $this->note = $data['tNote'];
        $this->amount = $data['amount'];
    }
    
    // data store into database
    public function store(){
        if(isset($this->date) && !empty($this->date) &&
           isset($this->type) && !empty($this->type) &&
           isset($this->note) && !empty($this->note) &&
           isset($this->amount) && !empty($this->amount)){
            
            $qry = "INSERT INTO `getcost`.`transaction` (`id`, `date`, `type`, `note`, `amount`) "
                . "VALUES (NULL, '".$this->date."', '".$this->type."', '".$this->note."', '".$this->amount."') ";
            
            if(mysql_query($qry)){
                $_SESSION['msg'] = '<font color="green">'." Transaction Added. ".'</font>';
                header("location: create.php");
            }else{
                $_SESSION['msg'] = '<font color="red">'."Error while transactioned. ".'</font>';
                header("location: create.php");
            }
            
           
        }else{
            $_SESSION['msg'] = '<font color="red">'."Value not Set. ".'</font>';
            header("location: create.php");
        }
    }
    
    // data read from database
    
    public function read(){
        $read = "SELECT * FROM `transaction` ORDER BY `date` DESC ";
        $qry = mysql_query($read);
        $row = mysql_num_rows($qry);
        
        if($row>0){
            //echo "get data ";
            echo '<table border="1">';
            echo '<td>Date</td>';
            echo '<td>Type</td>';
            echo '<td>Note</td>';
            echo '<td>Amount</td>';
            while($rec = mysql_fetch_object($qry)){
                echo '<tr>';
                echo '<td>'.$rec->date.'</td>';
                echo '<td>';
                    if($rec->type == "1"){
                        echo 'Balance';
                    }elseif($rec->type == "2"){
                        echo "Cost";
                    }else{
                        echo "N/A";
                    }
                echo '</td>';
                echo '<td>'.  ucfirst($rec->note).'</td>';
                echo '<td>'.$rec->amount.'</td>';
                echo '</tr>';
            }
            echo '</table>';
            
            
            $query = "SELECT type, SUM(amount) FROM transaction GROUP BY type"; 
	 
            $result = mysql_query($query) or die(mysql_error());

            // Print out result
            
            echo '<br/>';
            while($row = mysql_fetch_array($result)){
                    echo "Total "; 
                    if($row['type'] == 1){
                        echo "Balance";
                    }else{
                        echo "Cost";
                    }
                        
                    echo " = ". $row['SUM(amount)'];
                    echo "<br />";
            }
            
            echo '<a href="create.php">Add New Transaction</a>';
            
            
        }else{
            echo "Sorry there are no data in database";
        }
    }
    
    
    
    
    
}
